
## Fetch

```
git clone git@gitlab.com:eyeplusplus/ecosia-api.git
cd ecosia
npm install
```

## Start
```
npm start
```

## Run tests
```
npm test
```
