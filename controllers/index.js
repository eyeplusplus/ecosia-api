const errors = require('../lib/errors');

const handler = (req, res, next) => {
    try {
        const body = JSON.parse(req.body);
        const { favoriteTree } = body;
        let htmlContent = '';
        if (favoriteTree) {
            htmlContent = `It's nice to know that your favorite tree is a ${favoriteTree}`;
        } else {
            htmlContent = 'Please tell me your favorite tree';
        }
        const resBody = `<!DOCTYPE html>
        <html lang="en">
            <head></head>
            <body>
                ${htmlContent}
            </body>
        </html>`;

        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(resBody),
            'Content-Type': 'text/html'
        });
        res.end(resBody);
    } catch (e) {
        next(new errors.BadRequestError());
    }
}

module.exports = handler;
