const http = require('http');
const PORT = process.env.PORT || 8000;
const errors = require('./lib/errors');
const bodyParser = require('./middleware/body-parser');
const indexCtrl = require('./controllers/index');

const middleware = [
    bodyParser
];

const routes = [
    { method: 'POST', url: '/', handler: indexCtrl },
];

const server = http.createServer((req, res) => {
    try {

        const _reqMethod = req.method;
        const _reqUrl = req.url;

        // Set response headers
        res.setHeader('Content-Type', 'application/json');

        if (!_reqMethod || !_reqMethod.toLowerCase) {
            throw new errors.UnsupportedMethodError()
        }

        // Route handlers
        let r = 0;
        const _runRouteHandler = (err) => {
            if (err) {
                res.statusCode = err.status;
                res.end(JSON.stringify(err.body));
            }

            const routeConfig = routes[r++];

            if (routeConfig) {
                const matchedMethod = routeConfig.method.toLowerCase() === _reqMethod.toLowerCase();
                const matchedRoute = routeConfig.url === _reqUrl;

                if (matchedMethod && matchedRoute) {
                    return routeConfig.handler(req, res, _runRouteHandler);
                }
            }

            // Send a default response if not already sent
            try {
                res.statusCode = 404;
                res.end(JSON.stringify({ message: "No matching API found." }));
            } catch (e) {
                // Swallow response already sent error
            }
        };

        // Middleware processors
        let i = 0;
        const _runMiddleware = (err) => {
            if (err) {
                res.statusCode = err.status;
                res.end(JSON.stringify(err.body));
            }
            const hdlr = middleware[i++];
            if (hdlr) {
                return hdlr(req, res, _runMiddleware);
            }
            return _runRouteHandler();
        };

        _runMiddleware();

    } catch (e) {
        res.statusCode = e.status;
        res.end(JSON.stringify(e.body));
    }
});

server.listen(PORT);
console.log(`Server started on ${PORT}`);
module.exports = server;
