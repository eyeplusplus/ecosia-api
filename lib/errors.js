'use strict';

const util = require('util');

const errorMap = {
    UnsupportedMethodError: {
        statusCode: 405,
        message: 'Method not allowed'
    },
    BadRequestError: {
        statusCode: 400,
        message: 'Bad request'
    }
};

module.exports = (function setUpErrors () {
	const errors = {};

    for (const key in errorMap) {
        const value = errorMap[key];
        const localErrorFunc = new Function('value', 'key',
            `return function ${key} (errorMessage, errorData) {
                this.status = value.statusCode;
                this.name = key;
                this.body = { result: errorMessage || value.message, data: errorData || []};
            }`)(value, key);
		util.inherits(localErrorFunc, Error);
		errors[key] = localErrorFunc;
    }

	return errors;
}());
