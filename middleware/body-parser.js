module.exports = (req, res, next) => {
    let body = '';
    req.on('data', (chunk) => {
        body += chunk.toString();
    });

    req.on('end', () => {
        req.body = body;
        next(null);
    });
};