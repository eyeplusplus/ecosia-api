const request = require('request');
const util = require('util');
const requestAsync = util.promisify(request);
const PORT = process.env.PORT || 8000;
const server = require('../../index');

beforeAll(() => {
    if (server.listening) {
        return true;
    }
});

afterAll(() => {
    server.close();
});

test('returns BadRequestError', async () => {
    const errorResponse = await requestAsync({
        url: `http://localhost:${PORT}/`,
        method: 'POST',
        json: true,
    })
    expect(errorResponse.statusCode).toBe(400);
    expect(errorResponse.body).toMatchSnapshot();
})
test('asks for favorite tree', async () => {
    const response = await requestAsync({
        url: `http://localhost:${PORT}/`,
        method: 'POST',
        body: {},
        json: true,
    })
    expect(response.statusCode).toBe(200);
    expect(response.body).toMatchSnapshot();
})
test('displays favorite tree', async () => {
    const response = await requestAsync({
        url: `http://localhost:${PORT}/`,
        method: 'POST',
        body: {favoriteTree: 'baobab'},
        json: true,
    })
    expect(response.statusCode).toBe(200);
    expect(response.body).toMatchSnapshot();
})